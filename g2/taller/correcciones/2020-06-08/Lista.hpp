#include "Lista.h"

Lista::Lista() : _long(0), _head(nullptr), _tail(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* temp = _head;
    while (temp) {
        _head = temp->sig;
        delete temp;
        temp = _head;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* ptr1 = aCopiar._head;
    for (int i = 0; i < this->_long ; ++i) {
        this->eliminar(i);
    }
    this->_long = 0;
    while (ptr1) {
        this->agregarAtras(ptr1->data);
        ptr1 = ptr1->sig;
    }
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo = new Nodo();
    nodo->data = elem;
    nodo->sig = this->_head;
    nodo->ant = nullptr;
    this->_head = nodo;
    if (nodo->sig != nullptr){
        nodo->sig->ant = nodo;
    } else {
        this->_tail = nodo;
    }
    this->_long++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo = new Nodo();
    nodo->data = elem;
    nodo->sig = nullptr;
    nodo->ant = this->_tail;
    this->_tail = nodo;
    if (nodo->ant != nullptr) {
        nodo->ant->sig = nodo;
    } else {
        this->_head = nodo;
    }
    this->_long++;

}

void Lista::eliminar(Nat i) {
    if (i == 0){
        if (_head->sig == nullptr){
            _head = nullptr;
            _long--;
            delete _head;
            delete _tail;
        } else {
            Nodo* ptr = this->_head;
            this->_head = _head->sig;
            _head->ant = nullptr;
            delete ptr;
            _long--;
        }
    } else if (i == _long - 1) {
        Nodo* ptr = this->_tail;
        this->_tail = _tail->ant;
        _tail->sig = nullptr;
        _long--;
        delete ptr;
    } else {
        int j = 0;
        Nodo* temp = this->_head;
        while (j < i && temp != nullptr) {
            temp = temp->sig;
            j++;
        }
        temp->sig->ant = temp->ant;
        temp->ant->sig = temp->sig;
        delete temp;
        _long--;
    }
}

int Lista::longitud() const {
    return _long;
}

const int& Lista::iesimo(Nat i) const {
    int j = 0;
    Nodo* temp = _head;
    while (j < i && temp != nullptr) {
        temp = temp->sig;
        j++;
    }
    return temp->data;
}

int& Lista::iesimo(Nat i) {
    int j = 0;
    Nodo* temp = _head;
    while (j < i && temp != nullptr) {
        temp = temp->sig;
        j++;
    }
    return temp->data;
}


void Lista::mostrar(ostream& o) {
    // Completar
}
