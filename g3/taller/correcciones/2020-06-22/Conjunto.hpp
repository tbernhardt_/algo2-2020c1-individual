
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr) {}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return busqueda_aux(_raiz, clave);
}



template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)) {
        Nodo* nodo_clave = new Nodo(clave);
        Nodo* y = nullptr;
        Nodo* temp = _raiz;
        while (temp != nullptr) {
            y = temp;
            if (clave < temp->valor) {
                temp = temp->izq;
            } else {
                temp = temp->der;
            }
        }
        nodo_clave->padre = y;
        if (y == nullptr) {
            _raiz = nodo_clave;
        } else {
            if (clave < y->valor) {
                y->izq = nodo_clave;
            } else {
                y->der = nodo_clave;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* y = _raiz;
    while (y->valor != clave) {
        if (clave < y->valor) {
            y = y->izq;
        } else {
            y = y->der;
        }
    }
    Nodo* nodo_clave = y;
    if (y->izq != nullptr && y->der != nullptr) {
        Nodo* temp = _raiz;
        const T& sig = this->siguiente(clave);
        while (temp->valor != sig) {
            if (sig < temp->valor) {
                temp = temp->izq;
            } else {
                temp = temp->der;
            }
        }
        y = temp;
    }
    Nodo* x;
    if (y->izq != nullptr) {
        x = y->izq;
    } else {
        x = y->der;
    }
    if (x != nullptr) {
        x->padre = y->padre;
    }
    if (y->padre == nullptr) {
        _raiz = x;
    } else {
        if (y == y->padre->izq) {
            y->padre->izq = x;
        } else {
            y->padre->der = x;
        }
    }
    if (y->valor != clave) {
        nodo_clave->valor = y->valor;
    }
    delete y;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    if (this->pertenece(clave)) {
        Nodo* temp = _raiz;
        while (temp->valor != clave) {
            if (clave < temp->valor) {
                temp = temp->izq;
            } else {
                temp = temp->der;
            }
        }
        if (temp->der != nullptr) {
            return minimo_desde(temp->der);
        }
        Nodo* padre = temp->padre;
        while (padre != nullptr && temp == padre->der) {
            temp = padre;
            padre = padre->padre;
        }
        return padre->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    if (_raiz->izq == nullptr) {
        return _raiz->valor;
    } else {
        Nodo* temp = _raiz->izq;
        while (temp->izq != nullptr) {
            temp = temp->izq;
        }
        return temp->valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    if (_raiz->der == nullptr) {
        return _raiz->valor;
    } else {
        Nodo* temp = _raiz->der;
        while (temp->der != nullptr) {
            temp = temp->der;
        }
        return temp->valor;
    }
}


template <class T>
unsigned int Conjunto<T>::cardinal() const {
    if (_raiz == nullptr) {
        return 0;
    } else {
        return cardinal_aux(_raiz);
    }
}


template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template <class T>
unsigned int Conjunto<T>::cardinal_aux(Nodo *nodo) const {
    if (nodo == nullptr) {
        return 0;
    }
    unsigned int res;
    res = cardinal_aux(nodo->izq) + cardinal_aux(nodo->der) + 1;
    return res;
}

template <class T>
void Conjunto<T>::destruir(Nodo *nodo) {
    if (nodo) {
        destruir(nodo->izq);
        destruir(nodo->der);
        delete nodo;
    }
}

template <class T>
bool Conjunto<T>::busqueda_aux(Nodo *nodo, const T& clave) const {
    if (nodo == nullptr) {
        return false;
    }
    if (clave == nodo->valor) {
        return true;
    }
    if (clave < nodo->valor) {
        busqueda_aux(nodo->izq, clave);
    } else {
        busqueda_aux(nodo->der, clave);
    }
}

template <class T>
const T& Conjunto<T>::minimo_desde(Nodo *nodo) const {
    if (nodo->izq == nullptr) {
        return nodo->valor;
    } else {
        return minimo_desde(nodo->izq);
    }
}

