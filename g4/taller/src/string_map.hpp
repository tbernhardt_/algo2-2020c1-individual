
#include "string_map.h"

template<typename T>
string_map<T>::string_map() : _raiz(nullptr), _size(0) {};

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    for (string def : _claves) {
        this->erase(def);
    }
    for (string def : d._claves) {
        this->insert(make_pair(def, d.at(def)));
    }
}

template<typename T>
string_map<T>::~string_map() {
    destruir(_raiz);
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {
    // COMPLETAR
    // es opcional
}


template<typename T>
int string_map<T>::count(const string &clave) const {
    if (_raiz == nullptr) {
        return 0;
    } else {
        Nodo *temp = _raiz;
        int i = 0;
        while (temp && i < clave.length()) {
            if (temp->siguientes[clave[i]] == nullptr) {
                return 0;
            } else {
                temp = temp->siguientes[clave[i]];
                i++;
            }
        }
        if (i < clave.length()) {
            return 0;
        } else {
            if (temp->definicion != nullptr) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo *temp = _raiz;
    for (int i = 0; i < clave.length(); ++i) {
        temp = temp->siguientes[clave[i]];
    }
    return *temp->definicion;
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo *temp = _raiz;
    for (int i = 0; i < clave.length(); ++i) {
        temp = temp->siguientes[clave[i]];
    }
    return *temp->definicion;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    Nodo *temp = _raiz;
    for (int i = 0; i < clave.length(); ++i) {
        temp = temp->siguientes[clave[i]];
    }
    if (tieneHijos(temp)) {
        T* def = temp->definicion;
        temp->definicion = nullptr;
        delete def;
    } else {
            borrarAux(temp, clave, clave.length());
        _size--;
        for (string def : _claves) {
            if (clave == def) {
                _claves.erase(def);
                break;
            }
        }
    }
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    return _size == 0;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &def) {
    if (_raiz == nullptr) {
        _raiz = new Nodo();
        insertAux(_raiz, def, 0);
        _size++;
        _claves.insert(def.first);
    } else {
        Nodo *temp = _raiz;
        int i = 0;
        string clave = def.first;
        if (count(clave) == 0) {
            while (i < clave.length()) {
                if (temp->siguientes[clave[i]] == nullptr) {
                    temp->siguientes[clave[i]] = new Nodo();
                    insertAux(temp->siguientes[clave[i]], def, i + 1);
                    break;
                } else {
                    temp = temp->siguientes[clave[i]];
                    i++;
                }
            }
            _size++;
            _claves.insert(clave);
        } else {
            while (i < clave.length()) {
                temp = temp->siguientes[clave[i]];
                i++;
            }
            *temp->definicion = def.second;
        }
    }
}

template<typename T>
void string_map<T>::insertAux(Nodo *nodo, const pair<string, T> &def, int i) {
    Nodo *temp = nodo;
    string clave = def.first;
    while (i < clave.length()) {
        if (i < clave.length() - 1) {
            temp->siguientes[clave[i]] = new Nodo();
            temp = temp->siguientes[clave[i]];
            i++;
        } else {
            temp->siguientes[clave[i]] = new Nodo();
            temp = temp->siguientes[clave[i]];
            temp->definicion = new T(def.second);
            i++;
        }
    }
}

template<typename T>
bool string_map<T>::tieneHijos(Nodo *nodo) {
    for (int i = 0; i < 256; ++i) {
        if (nodo->siguientes[i] != nullptr) {
            return true;
        }
    }
    return false;
}

template<typename T>
void string_map<T>::borrarAux(Nodo *nodo, string clave, int largo) {
    if (nodo != _raiz) {
        Nodo *padre = _raiz;
        for (int i = 0; i < largo - 1; ++i) {
            padre = padre->siguientes[clave[i]];
        }
        Nodo* temp = padre->siguientes[clave[largo - 1]];
        if (!tieneHijos(nodo)) {
            if (nodo->definicion) {
                delete nodo->definicion;
            }
            nodo = nullptr;
            padre->siguientes[clave[largo - 1]] = nullptr;
            delete temp;
            borrarAux(padre, clave, largo - 1);
        } else {
            if (nodo->definicion) {
                delete nodo->definicion;
            }
        }
    }
}

template<typename T>
void string_map<T>::destruir(Nodo *nodo) {
    if (nodo) {
        for (Nodo *hijo : nodo->siguientes) {
            if (hijo) {
                destruir(hijo);
            }
        }
        if (nodo->definicion) {
            delete nodo->definicion;
        }
        delete nodo;
    }
}