#include <iostream>


using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    void incrementar_dia();

  private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia) :  mes_(mes), dia_(dia){}

int Fecha::mes(){
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}


#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia() {
    if(dia_ != dias_en_mes(mes_)){
        dia_++;
    } else {
        dia_ = 1;
        mes_++;
    }
}

// Ejercicio 11, 12

class Horario {
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario o);
        bool operator<(Horario o);
        //bool operator!=(Horario o); lo dejo como comentario por si lo llego a necesitar para algo
    private:
        int hora_;
        int min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

bool Horario::operator==(Horario o) {
    bool igual_hora = this->hora() == o.hora();
    bool igual_min = this->min() == o.min();
    return igual_hora && igual_min;
}

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario o) {
    bool menor_hora = this->hora() < o.hora();
    bool igual_hora = this-> hora() == o.hora();
    bool menor_min = this->min() < o.min();
    if(igual_hora){
        return menor_min;
    } else {
        if(menor_hora){
            return true;
        } else {
            return false;
        }
    }
}

/*      implementacion de operador != para horarios

bool Horario::operator!=(Horario o) {
    bool distinta_hora = this->hora() != o.hora();
    bool distinto_min = this->min() != o.min();
    return distinta_hora || distinto_min;
}
*/


// Ejercicio 13

class Recordatorio{
    public:
        Recordatorio(Fecha fecha, Horario horario, string mensaje);
        //bool operator<(Recordatorio o);  lo dejo por si me llega a servir
        Fecha fecha();
        Horario horario();
        string mensaje();
    private:
        Fecha fecha_;
        Horario horario_;
        string mensaje_;

};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje) : fecha_(fecha), horario_(horario), mensaje_(mensaje) {}

Fecha Recordatorio::fecha() {
    return fecha_;
}

Horario Recordatorio::horario() {
    return horario_;
}

string Recordatorio::mensaje() {
    return mensaje_;
}

/*    implementacion de operador < para recordatorios
bool Recordatorio::operator<(Recordatorio o) {      //Distingue por horario dos recordatorios
    bool temprano = this->horario() < o.horario();
    return temprano;
}
*/
ostream& operator<<(ostream& os, Recordatorio r){
    os << r.mensaje() << " " << "@" << " " << r.fecha() << " " << r.horario();
    return os;
}


// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        void ordenar_recs();
        vector<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();
    private:
        Fecha hoy_;
        vector<Recordatorio> recs_;

};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial) {}

Fecha Agenda::hoy() {
    return hoy_;
}

void Agenda::incrementar_dia() {
    return hoy_.incrementar_dia();
}

void  Agenda::ordenar_recs() {          //Bubble sort
    for (int i = 0; i < recs_.size()-1 ; i++) {
        bool ordenado = false;
        for (int j = 0; j < recs_.size()-i-1; j++) {
            if (!(recs_[j].horario() < recs_[j+1].horario())) {
                swap(recs_[j], recs_[j+1]);
                ordenado = true;
            }
        }
        if (!ordenado) {
            break;
        }
    }
}


vector<Recordatorio> Agenda::recordatorios_de_hoy() {
    vector<Recordatorio> recs_de_hoy;
    for (int i = 0; i < recs_.size() ; ++i) {
        if(recs_[i].fecha() == hoy_){
            recs_de_hoy.push_back(recs_[i]);
        }
    }
    return recs_de_hoy;
}


void Agenda::agregar_recordatorio(Recordatorio rec) {
    recs_.push_back(rec);
}

ostream& operator<<(ostream& os, vector<Recordatorio> recs){
    for (int i = 0; i < recs.size() ; ++i) {
        os <<  recs[i] << endl;
    }
    return os;
}



ostream& operator<<(ostream& os, Agenda a){
    a.ordenar_recs();
    os << a.hoy() << endl << "=====" << endl << a.recordatorios_de_hoy();
    return os;
}
